1. Merge/pull request (как работает, как создать и зачем нужен) 
    Merge/pull request - это процесс, при котором разработчик отправляет свои изменения в коде на проверку другому разработчику для их слияния с основной кодовой базой.
    Чтобы создать запрос на слияние проделанных изменений необходимо:
    - создать форк (копию) репозитория
    - создать новую ветку в этой копии и внести в нее свои изменения
    - отправить запрос (pull request) на слияние созданной ветки с основной в оригинальном репозитории, после чего инженер (разрабочтик), ответственный за слияние производит его.
    
2. Rebase (как работает, как изменить коммит мессадж, как подтянуть изменения в свою ветку из родительской)
    Rebase вносит изменения в историю коммитов, путем перемещения коммитов из одной ветки в другую.

3. Магические файлы (.gitignore) 
    Расширение файла .gitignore используется для указания файлов и директорий, которые должны быть проигнорированы при выполнении операций коммита, добавления и отправки файлов на удаленный репозиторий.

4. Как отменить последний commit?
    При помощи команды git reset. 
    Выполнение команды без дополнительных параметров приведет к отмене последнего коммита, при этом сохранить все внесенные изменения в рабочей директории.
    Параметр --soft позволяет сохранить все изменения в рабочей директории и индексе.
    Параметр --hard отменяет последний коммит и удаляет все внесенные изменения, в том числе и из рабочей директории.

5. Для чего нужна команда git stash?
    Команда git stash используется для временного сохранения всех текущих изменений в рабочей директории и индексе, чтобы можно было переключиться на другую ветку или выполнить какие-либо другие операции без необходимости коммитить все изменения

6. Можно ли объединить несколько коммитов в один?
    Можно при помощи интерактивного режима команды rebase (git rebase -i), если вместо «pick» или «edit» указать «squash» 

7. Как отменить «git add» до коммита?
    При помощи команды git reset

8. Как изменить стандартный редактор в git? 
    При помощи команды  git config --global core.editor <название редактора>

9. Как сравнить две ветки и посмотреть изменения? А как сравнить 1 файл на 2 разных ветках? 
    При помощи команды git-diff. 
    Чтобы сравнить 1 файл на двух разных ветках использеум команду: git diff <ветка1> <ветка2> <файл>

10. Можно ли узнать, смерджена или нет ветка в master?
    Можно при помощи команды git branch --merged master

11. Что такое cherry-pick?
    Команда, с помощью которой можно выборочно применить коммиты к текущей master ветке

12. Как создать локальную ветку и отправить её на удалённый сервер?
    При помощи команды push вида: git push <удаленный сервер> <ветка>

13. Отличие git reset от git revert
    Команада git revert не удаляет коммит, а создает новый, содержащий удаленные изменения из коммита

14. Как посмотреть текущее состояние репозитория? 
    При помощи команды git-status

15. Как посмотреть историю измений?
    При помощи команды git log

16. Для чего может понадобиться mergetool?
    Для решения конфликтов при слиянии веток и файлов

